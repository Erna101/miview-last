const mongoose = require("mongoose");
mongoose.set("debug", true);
mongoose.Promise = Promise;
mongoose.connect("mongodb://localhost/miView2",{
    keepAlive: true
});

module.exports.User = require("./user");
module.exports.Inquiry = require("./inquiry");